const ENEMY = 'enemy';

class Enemy extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y, hero, bullets, killHero, killEnemy) {
        super(scene, x, y, ENEMY);

        this.scene = scene;

        this.scene.physics.world.enable(this);
        this.scene.add.existing(this);

        this.rotation = Phaser.Math.Angle.Between(x, y, hero.x, hero.y);

        const SPEED = 100;
        this.body.velocity.x = SPEED * Math.cos(this.rotation);
        this.body.velocity.y = SPEED * Math.sin(this.rotation);

        this.play(ENEMY, true);

        scene.physics.add.collider(
            hero,
            this,
            (hero, me) => killHero.apply(scene, hero, me)
        );

        for (let bullet of bullets) {
            scene.physics.add.collider(
                this,
                bullet,
                killEnemy
            );
        }
    }
};

export { ENEMY, Enemy };
