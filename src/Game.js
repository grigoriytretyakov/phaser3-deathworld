import { BULLET, Bullet } from './Bullet.js';
import { ENEMY, Enemy } from './Enemy.js';
import { HERO, Hero } from './Hero.js';

const BACKGROUND = 'bg';

class Game extends Phaser.Scene {
    constructor() {
        super({key: 'Game'});

        this.HEAD_GUN_DIST = Math.sqrt(20 * 20 + 16.5 * 16.5);
        this.GUN_ANGLE = Math.atan(16.5 / 20);
        this.GUN_RELOAD_TIME = 200;

        this.tick = 0;
        this.fire = false;

        this.mx = 0;
        this.my = 0;

        this.bullets = [];
        this.enemies = [];
    }

    preload() {
        this.load.image(BACKGROUND, 'assets/bg.png');
        this.load.spritesheet(
            HERO, 'assets/hero.png',
            {
                frameWidth: 41,
                frameHeight: 47
            }
        );
        this.load.spritesheet(
            ENEMY, 'assets/enemy.png',
            {
                frameWidth: 32,
                frameHeight: 32
            }
        );
        this.load.image(BULLET, 'assets/bullet.png');
    }

    create() {
        this.add.image(350, 300, BACKGROUND);

        this.anims.create({
            key: ENEMY,
            frames: this.anims.generateFrameNumbers(ENEMY, {start: 0, end: 2}),
            frameRate: 8,
            repeat: -1
        });

        this.anims.create({
            key: HERO,
            frames: this.anims.generateFrameNumbers(HERO, {start: 0, end: 2}),
            frameRate: 8,
            repeat: -1
        });
        this.hero = new Hero(this, 350, 300);

        this.input.on('pointerdown', (event) => {
            this.fire = true;
            this.mx = event.x;
            this.my = event.y;
        }, this);

        this.input.on('pointermove', (event) => {
            this.mx = event.x;
            this.my = event.y;
        }, this);

        this.input.on('pointerup', (event) => {
            this.fire = false;
        }, this);

        this.time.addEvent({
            delay: 500,
            callback: this.createEnemy,
            callbackScope: this,
            loop: true
        });

        this.createEnemy();
    }

    createEnemy() {
        const x = Phaser.Math.Between(-50, 750);
        const y = x > 0 && x < 750
            ? Phaser.Math.RND.pick([-25, 625])
            : Phaser.Math.Between(0, 600);

        this.enemies.push(new Enemy(
            this,
            x,
            y,
            this.hero,
            this.bullets,
            this.killHero,
            this.killEnemy
        ));
    }
    
    killEnemy(enemy, bullet) {
        enemy.destroy();
        bullet.destroy();
    }

    killHero(hero, enemy) {
        this.scene.start('Menu');
    }

    update(time, delta) {
        let angle = Phaser.Math.Angle.Between(this.hero.x, this.hero.y, this.mx, this.my);
        this.hero.rotation = angle;
        if (Math.abs(this.hero.x - this.mx) < 1 && Math.abs(this.hero.y - this.my) < 1) {
            this.hero.body.velocity.x = 0;
            this.hero.body.velocity.y = 0;
        }
        else {
            this.hero.body.velocity.x = 130 * Math.cos(angle);
            this.hero.body.velocity.y = 130 * Math.sin(angle);
        }

        this.tick += delta;

        const gun_angle = this.GUN_ANGLE + angle;

        if (this.tick >= this.GUN_RELOAD_TIME) {
            this.tick = this.GUN_RELOAD_TIME;
            if (this.fire) {
                this.tick = 0;

                let bullet = new Bullet(
                    this,
                    this.hero.x + this.HEAD_GUN_DIST * Math.cos(gun_angle),
                    this.hero.y + this.HEAD_GUN_DIST * Math.sin(gun_angle)
                );
                for (let e of this.enemies) {
                    this.physics.add.collider(e, bullet, (enemy, bullet) => this.killEnemy(enemy, bullet));
                }
                this.bullets.push(bullet);
                bullet.rotation = angle;
                bullet.body.velocity.x = bullet.MAX_SPEED * Math.cos(angle);
                bullet.body.velocity.y = bullet.MAX_SPEED * Math.sin(angle);
            }
        }

        let bullets = [];
        for (let bullet of this.bullets) {
            if (bullet.x > -10 && bullet.x < 710 & bullet.y > -10 && bullet.y < 610) {
                bullets.push(bullet);
            }
            else{
                bullet.destroy();
            }
        }
        this.bullets = bullets;

        let enemies = [];
        for (let enemy of this.enemies) {
            if (enemy.x > -50 && enemy.x < 750 & enemy.y > -50 && enemy.y < 650) {
                enemies.push(enemy);
            }
            else{
                enemy.destroy();
            }
        }
        this.enemies = enemies;
    }
}

export { Game };
