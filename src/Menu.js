class Menu extends Phaser.Scene {
    constructor() {
        super({key: 'Menu'});
    }

    preload() {
        this.load.image('start', 'assets/start.png');
    }

    create() {
        let btn = this.add.sprite(350, 300, 'start').setInteractive();
        btn.on('pointerdown', () => { this.scene.start('Game'); });
    }
}

export { Menu };
