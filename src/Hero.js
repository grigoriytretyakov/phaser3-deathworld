const HERO = 'hero';

class Hero extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, HERO)
        this.scene = scene;

        this.scene.physics.world.enable(this);
        this.scene.add.existing(this);

        this.play(HERO, true);
    }

};

export { HERO, Hero };
