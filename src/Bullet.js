
const BULLET = 'bullet';

class Bullet extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, BULLET);

        this.MAX_SPEED = 400;

        this.scene = scene;

        this.scene.physics.world.enable(this);
        this.scene.add.existing(this);
    }

};

export { BULLET, Bullet };
