import 'phaser';

import { Menu } from './Menu.js';
import { Game } from './Game.js';


const config = {
    type: Phaser.AUTO,
    parent: 'game',
    width: 700,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false
        }
    },
    scene: [
        Menu,
        Game,
    ]
}

new Phaser.Game(config);
